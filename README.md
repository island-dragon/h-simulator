# Tiny ExcaliburJS scaffold

Bare minimum for a TypeScript based ExcaliburJS project with tiny sample.

Once loaded you will see a Excalibur screen with a play button. Once you play a blue bubble will follow your mouse. That's all this project does.

## Files

- dist/ - Will be created when you build
- src/ - Where all game related files are, ignore anything outside here
- src/index.ts - All game logic
- src/utils.ts - Utility things
- src/resources/index.ts - Definition for included resources
- src/resources/images/bubble.png - Sample resource to be loaded

## Install

- Clone repo
- run `npm i`
- run `npm start`

## Build

- run `npm run build`
