import * as ex from 'excalibur'
import { Loader, SpriteSheets } from './resources'

const game = new ex.Engine({
  backgroundColor: ex.Color.Black,
  displayMode: ex.DisplayMode.FullScreen,
  suppressPlayButton: true,
});

const gemSprites = {
  green: SpriteSheets.Items['gemGreen'],
  blue: SpriteSheets.Items['gemBlue'],
  yellow: SpriteSheets.Items['gemYellow'],
  orange: SpriteSheets.Items['gemRed'],
}

const gemSpritesNames = Object.keys(gemSprites)

const level = `
  0 1 0
  1 0 1
  0 1 0
`.replace(/\s/g, '')

class Node {
  constructor(
    public actor: ex.Actor,
    public l?: Node,
    public u?: Node,
    public r?: Node,
    public d?: Node,
  ) { }
}

const nodes: Node[] = []

const levelWidth = 3
const spriteWidth = 64
const spacing = spriteWidth + 16
const marginX = game.halfDrawWidth - spacing * (levelWidth - 1) / 2
const marginY = game.halfDrawHeight - spacing * (levelWidth - 1) / 2
const wiggle = 20

const addGem = (x: number, y: number, type: ex.Sprite): ex.Actor => {
  const gem = new ex.Actor(x, y, spriteWidth, spriteWidth)
  gem.addDrawing(type)

  return gem
}

let target: ex.Actor
let offSetX = 0
let offSetY = 0
let startX = 0
let startY = 0

const start = (e: ex.Input.PointerDragEvent) => {
  target = e.target
  target.setZIndex(1)
  offSetX = target.pos.x - e.screenPos.x
  offSetY = target.pos.y - e.screenPos.y
  startX = target.pos.x
  startY = target.pos.y
}
const move = (e: ex.Input.PointerDragEvent) => {
  if (!target) return

  const x = e.pointer.lastScreenPos.x + offSetX - startX
  const y = e.pointer.lastScreenPos.y + offSetY - startY

  const absX = Math.abs(x)
  const absY = Math.abs(y)

  const adjustedX = Math.max(startX - spacing, Math.min(startX + spacing, (absX >= absY) ? e.pointer.lastScreenPos.x + offSetX : startX))
  const adjustedY = Math.max(startY - spacing, Math.min(startY + spacing, (absX < absY) ? e.pointer.lastScreenPos.y + offSetY : startY))
  target.pos.setTo(adjustedX, adjustedY)

  const node = nodes[target.id]
  if (node.l) {
    if (x < -wiggle) {
      node.l.actor.pos.setTo((startX - spacing) + startX - adjustedX, startY)
    } else {
      node.l.actor.pos.setTo(startX - spacing, startY)
    }
  }
  if (node.r) {
    if (x > wiggle) {
      node.r.actor.pos.setTo((startX + spacing) + startX - adjustedX, startY)
    } else {
      node.r.actor.pos.setTo(startX + spacing, startY)
    }
  }
  if (node.u) {
    if (y < -wiggle) {
      node.u.actor.pos.setTo(startX, (startY - spacing) + startY - adjustedY)
    } else {
      node.u.actor.pos.setTo(startX, startY - spacing)
    }
  }
  if (node.d) {
    if (y > wiggle) {
      node.d.actor.pos.setTo(startX, (startY + spacing) + startY - adjustedY)
    } else {
      node.d.actor.pos.setTo(startX, startY + spacing)
    }
  }
}
const end = (e: ex.Input.PointerDragEvent) => {
  if (!target) return

  target.setZIndex(0)
  target.pos.setTo(startX, startY)
  target = null
}

for (let y = 0; y < level.length / levelWidth; y++) {
  for (let x = 0; x < levelWidth; x++) {
    const gem = addGem(
      marginX + x * spacing,
      marginY + y * spacing,
      gemSprites[gemSpritesNames[level[x + y * levelWidth]]]
    )

    gem.on(ex.EventTypes.PointerDragStart, start)
    gem.on(ex.EventTypes.PointerDragMove, move)

    const i = x + y * levelWidth
    nodes.push(new Node(gem))
    if (x > 0) {
      nodes[i].l = nodes[i - 1]
      nodes[i - 1].r = nodes[i]
    }
    if (y > 0) {
      nodes[i].u = nodes[i - levelWidth]
      nodes[i - levelWidth].d = nodes[i]
    }

    game.add(gem)
  }
}

game.input.pointers.primary.on(ex.EventTypes.Up, end)

game.start(Loader)
