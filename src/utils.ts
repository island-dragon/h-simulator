import * as ex from 'excalibur'

export const asg = Object.assign

export const fromXmlToSpriteSheet = function (tex: any, meta: any) {
  const spriteSheet = {}
  for (const t of meta.TextureAtlas.SubTexture) {
    const { name, x, y, width, height } = t['$']
    spriteSheet[name.replace('.png', '')] = new ex.Sprite(
      tex,
      x,
      y,
      width,
      height
    )
  }

  return spriteSheet
}
