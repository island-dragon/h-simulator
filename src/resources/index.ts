import * as ex from 'excalibur'
import { fromXmlToSpriteSheet } from '../utils'

const itemsMeta = require('./images/spritesheet_items.xml')

let Resources = {
  Bubble: new ex.Texture(require('./images/bubble.png')),
  Items: new ex.Texture(require('./images/spritesheet_items.png')),
}

let SpriteSheets = {
  Items: fromXmlToSpriteSheet(Resources.Items, itemsMeta)
}

let Loader = new ex.Loader();
Loader.addResources(Object.values(Resources))

export { Loader, Resources, SpriteSheets }
